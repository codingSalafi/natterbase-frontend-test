import {mount} from '@vue/test-utils'
import Home from '@/components/Home'

import axios from "axios";
const MockAdapter = require("axios-mock-adapter");
const mock = new MockAdapter(axios);



describe('Home.vue', () => {
    afterAll(() => mock.restore());
  beforeEach(() => mock.reset());
    it("renders component", () => {
        const wrapper = mount(Home);
        expect(wrapper.element).toMatchSnapshot();
    })

    it("loads categories", async () => {
        mock
          .onGet("http://demo4507124.mockable.io/categories")
          .reply(200, [{ name: "foo" }, { name: "bar" }, { name: "baz" }]);
    
        const wrapper = shallowMount(Home);
        const catList = wrapper.$el.querySelector('.cat-list li')
        expect(catList).toHaveLength(3);
    });
    it("loads products", async () => {
        mock
          .onGet("http://demo4507124.mockable.io/products")
          .reply(200, [{ name: "foo" }, { name: "bar" }, { name: "baz" }]);
    
        const wrapper = shallowMount(Home);
        const prodList = wrapper.$el.querySelector('.cat-col')
        expect(prodList).toHaveLength(3);
      });
      
})

